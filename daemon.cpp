#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <termios.h>
#include <poll.h>

#include <string>
#include <vector>
#include <map>
#include <chrono>
using namespace std::literals::chrono_literals;
using Clock = std::chrono::steady_clock;

#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;
#include <concepts>

#include <cerrno>
#include <cstring>


#include <mcl/typedefs.h>
#include <mcl/args.h>
#include <mcl/miscutils.h>
#include <mcl/strmanip.h>


/* SETTINGS:
 * kMinRefr  : milliseconds
 * kSockPath : const char[]
 * kDevPath  : const char[]
 * kDelIncom : bool
 */
#include "config.h"


//TODO
#define USAGE \
	"usage: don't do it wrong.\n"



namespace parsers {
	using namespace mcl::args::supplied_parsers;
}


std::string showOpt(char c)
{
	return {{ '-', c }};
}
std::string showOpt(std::string s)
{
	return "--" + s;
}



bool processLines(int fd, std::string& part, bool a);


int main(int argc, char** argv)
{
	//Ensure only daemon



	//Process args

	enum Opts {
		help, socket, device
	};

	auto args = mcl::args::parse<Opts>(
			{
				{ "help",            help       },
				{ "socket",          socket     },
				{ "device",          device     },
				{ "incomplete-line", incomplete }
			},
			{
				{ 'h', help       },
				{ 's', socket     },
				{ 'd', device     },
				{ 'i', incomplete }
			},
			{
				{ socket, parsers::ftypeOrNexist(fs::file_type::socket) },
				{ device, parsers::filedes(O_WRONLY,
						1, fs::file_type::character) },
				{ incomplete,
					parsers::keyword<bool>(
						{ { "abandon", 1 }, { "use", 0 } }
					)
				}
			},
			argc, argv
		);


	fs::path sockp{kSockPath};
	int fd = -2;
	bool abandonIncom = kDelIncom ;

	for (auto& a : args)
	{
		if (a)
		{
			if (a.isLit())
			{
				std::cout << USAGE;
				return 1;
			}
			else switch (a.opt())
			{
			case help:
				std::cout << USAGE;
				return 0;
			case socket:
				sockp = a.as<fs::path>();
				break;
			case device:
				fd = a.as<int>();
				break;
			case incomplete:
				abandonIncom = a.as<bool>();
				break;
			}
		}
		else if (a.isUnk())
		{
			std::cout << "Unknown option: " <<
				std::visit([](auto o){return showOpt(o);},
						a.unkOptErr().opt) << '\n';
			return 1;
		}
		else switch (a.opt())
		{
		case help: break;
		case socket:
			std::cout << "Error parsing socket\n";
			return 1;
		case device:
			std::cout << "Error parsing device\n";
			return 1;
		case incomplete:
			std::cout << "Error parsing incomplete-line\n";
			return 1;
		}
	}





	if (fd == -2)
		fd = open(kDevPath, O_WRONLY);

	if (fd == -1)
	{
		std::cerr << "Bad fd.  errno: " << errno << "\n";
		return 2;
	}

	if (flock(fd, LOCK_EX | LOCK_NB) == -1)
	{
		std::cerr << "Failed to get lock on device.\n"
			"Ensure that no other program is writing to it.\n";
		return 2;
	}

	std::cout << "Using socket " << sockp << '\n';
	if (abandonIncom)
		std::cout << "Running with abandon\n";
	else
		std::cout << "Using incomplete lines\n";

	//Set up input method

	int lisnSock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (lisnSock == -1)
	{
		std::cerr << "TODO\n"; return 2;
	}

	sockaddr_un local;
	strcpy(local.sun_path, sockp.c_str());
	unlink(local.sun_path);

	bind(lisnSock, (sockaddr*)&local,
			sizeof(local.sun_family) + sockp.native().size());



	listen(lisnSock, 2);

	fcntl(lisnSock, F_SETFL
			fcntl(lisnSock, F_GETFL, 0));




	//Loop on input

	//- Bytes
	//- With offset
	//- Faders

	// TODO Factor to config
	const size_t pollsz = 15;
	pollfd polls[pollsz+1]{{-1, POLLIN, 0}};
	size_t pollc = 0;

	struct Info {
		sockaddr_un addr;
		std::string partial;
	} metas[pollsz];

	polls[pollsz].fd = lisnSock;
	++pollc;

	for(int count;;)
	{
		//How many fds are ready?
		count = poll(polls, pollsz+1, kMinRefr.count());

		if (count == -1)
		{
			std::cerr << "Uh Oh, stinky.\n";
			continue;
		}

		for (int i = 0; i < pollsz; i++)
		{
			// Test whether this socket is ready to read, or has hung up
			if (polls[i].revents & (POLLIN | POLLHUP))
			{
				if (processLines(polls[i].fd, metas[i].partial, abandonIncom))
				{
					close(polls[i].fd);
					polls[i].fd = -1;
				}
			}
		}

		//Get new connection
		if (polls[pollsz].revents & (POLLIN | POLLHUP))
		{
			int i = 0;
			// Fetch earliest availible spot
			while (polls[i].fd != -1)
				++i;

			//Do accept into the fetched spot,
			//test error.
			if (accept(polls[pollsz].fd,
					&metas[i].addr,
					sizeof(sockaddr_un)
				) == -1)
			{
				std::cerr << "Uh Oh, stinky.\n";
				continue;
			}
		}
	}

	return 0;
}




//Returns response
std::string processLine(const std::string& l)
{
	auto line = mcl::trimmed(l);

	if (!line.size())
		return "";

	char inst = line.front();

	line.remove_prefix(1);

	switch (line.front())
	{
	case '#': { //Set bytes
		size_t i = 0;
		byte newBytes[512]{0};
		bool zero = 0;
		if (line.back() == 'z') //fill rest with zeroes
		{
			zero = 1;
			line.pop_back();
		}
		//otherwise, keep rest as is

		for (char c : line)
		{
			int hval = mcl::hexval(c);
			if (hval != -1)
			{
				if (i & 1)
					newBytes[i >> 1] |= hval << 4;
				else
					newBytes[i >> 1] = hval
			}
			else if (std::isspace(c))
				continue;
			else
				return "Unrecognized char '" + mcl::ensurePrintable(c)
					+ "' in bytes.";
		}

		i = (i >> 1) + (i & 1);
		for (size_t j = 0; j < i; j++)
			GLOB_bytes[j+1] = newBytes[j];

		if (zero) while (i < 512)
		{
			GLOB_bytes[i+1] = newBytes[i];
			++i;
		}
	} break;
	case '@': { //With offset
		size_t i = 0;
		byte newBytes[512]{0};

		auto p = line.data();
		size_t o = *(size_t*)parsers::natural<size_t>()(p);



		for (char c : line)
		{
			int hval = mcl::hexval(c);
			if (hval != -1)
			{
				if (i & 1)
					newBytes[i >> 1] |= hval << 4;
				else
					newBytes[i >> 1] = hval
			}
			else if (std::isspace(c))
				continue;
			else
				return "Unrecognized char '" + mcl::ensurePrintable(c)
					+ "' in bytes.";
		}

		i = (i >> 1) + (i & 1);
		for (size_t j = 0; j < i; j++)
			GLOB_bytes[j+1] = newBytes[j];

		if (zero) while (i < 512)
		{
			GLOB_bytes[i+1] = newBytes[i];
			++i;
		}

	} break;
	case '<': //Fader
	case '>':

		break;
	case 's': //Report state

		break;
	case     '!': //ping
		return "!"; //pong
	default:
		return "Unrecognized instruction '" +
			mcl::ensurePrintable(line.front()) + "'";
	}
}



//TODO implement timeout
bool processLines(int fd, std::string& part, bool a)
{
	int ret = 0;
	for (char c; ret = read(fd, &c, 1); )
	{
		if (ret == 1)
		{
			if (c == '\n')
			{
				processLine(l);
				l.clear();
			}
			else
				l += c;
		}
		else //else make sure errno is a would block
			return 0;
	}

	if (a)
	{
		processLine(l);
		l.clear();
	}

	return 1;
}
